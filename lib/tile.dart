import 'package:flutter/material.dart';

class Tile extends StatefulWidget {
  const Tile(
      {Key? key,
      required this.child,
      required this.index,
      this.iswinner = false})
      : super(key: key);
  final Widget child;
  final bool iswinner;
  final int index;
  @override
  State<Tile> createState() => _TileState();
}

class _TileState extends State<Tile> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: !widget.iswinner
                ? const Color.fromRGBO(41, 75, 97, 1)
                : Colors.red,
            border: Border.all(
              width: 5,
              color: Colors.white,
            )),
        child: Padding(padding: const EdgeInsets.all(16), child: widget.child));
  }
}
