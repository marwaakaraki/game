import 'dart:math';
import 'package:flutter/material.dart';
import 'package:game/tile.dart';
import 'package:game/util.dart';
import 'dart:async';

class Game extends StatefulWidget {
  const Game({Key? key, required this.automate}) : super(key: key);
  final bool automate;
  @override
  State<Game> createState() => _GameState();
}

class _GameState extends State<Game> {
  xo turn = xo.X;
  StreamController<bool>? winner;
  List<int> xTiles = [];
  List<int> winnerTiles = [];
  List<int> oTiles = [];
  List<xo> tileStates = List.filled(9, xo.none);
  int start = 0;
  bool get getwinner =>
      evaluateWinnerr(xTiles).keys.first || evaluateWinnerr(oTiles).keys.first;

  winningTiles() {
    if (getwinner) {
      evaluateWinnerr(xTiles).values.forEach((element) {
        element.forEach((element) {
          winnerTiles.add(element);
        });
      });
      evaluateWinnerr(oTiles).values.forEach((element) {
        element.forEach((element) {
          winnerTiles.add(element);
        });
      });
    }
  }

  int randomNumber() {
    int numbers = Random().nextInt(9);
    if (tileStates[numbers] == xo.none) {
      return numbers;
    } else {
      int newNumbers = randomNumber();
      return newNumbers;
    }
  }

  @override
  void initState() {
    super.initState();
    winner = StreamController();
  }

  @override
  void dispose() {
    super.dispose();
    winner!.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: const Color.fromRGBO(41, 75, 97, 1),
          title:
              const Text('Tic Tac Toe', style: TextStyle(color: Colors.white)),
          leading: InkWell(
            child: const Icon(Icons.arrow_back, color: Colors.white),
            onTap: () => Navigator.pop(context),
          )),
      body: StreamBuilder(
          stream: winner!.stream,
          initialData: false,
          builder: (BuildContext ctx, AsyncSnapshot snapshot) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.width,
                    child: GridView.builder(
                        padding: const EdgeInsets.all(15),
                        itemCount: 9,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3),
                        itemBuilder: (context, index) => InkWell(
                              onTap: () => tapped(index, snapshot),
                              child: Tile(
                                index: index,
                                child: mapImage(tileStates[index]),
                                iswinner: winnerTiles.contains(index),
                              ),
                            ))),
                if (start == 9 && !snapshot.data)
                  const Text('Draw', style: TextStyle(color: Colors.white)),
                if (snapshot.data)
                  Text(
                      tileStates[winnerTiles.first] == xo.X ? 'X WON' : 'O WON',
                      style: const TextStyle(color: Colors.white)),
                if (start == 9 || snapshot.data)
                  MaterialButton(
                      onPressed: restart,
                      height: 40,
                      color: Colors.white,
                      child: const Text('play again',
                          style:
                              TextStyle(color: Color.fromRGBO(41, 75, 97, 1))))
              ],
            );
          }),
    );
  }

  void tapped(int index, AsyncSnapshot winnerr) {
    setState(() {
      if (tileStates[index] == xo.none) {
        turn == xo.X ? tileStates[index] = xo.X : tileStates[index] = xo.O;
        turn == xo.X ? xTiles.add(index) : oTiles.add(index);
        turn == xo.X ? turn = xo.O : turn = xo.X;
        winner!.add(getwinner);
        start++;
      }
    });
    if (widget.automate && !winnerr.data) {
      automateFunction(randomNumber());
    }
    winningTiles();
  }

  void restart() {
    winner!.add(false);

    turn = xo.X;
    setState(() {
      tileStates = List.filled(
        9,
        xo.none,
      );
      winnerTiles = [];
      xTiles = [];
      oTiles = [];
      start = 0;
    });
  }

  void automateFunction(int index) {
    setState(() {
      if (tileStates[index] == xo.none) {
        tileStates[index] = xo.O;
        oTiles.add(index);
        turn = xo.X;
        winner!.add(getwinner);
        start++;
      }
    });
    winningTiles();
  }
}
