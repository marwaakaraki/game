import 'package:flutter/material.dart';
import 'package:game/game.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'tic tac toe',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        scaffoldBackgroundColor: const Color.fromRGBO(41, 75, 97, 1),
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Tic Tac Toe',
                style: TextStyle(color: Colors.white, fontSize: 40)),
            MaterialButton(
              height: 40,
              color: Colors.white,
              child: const Text('play against me',
                  style: TextStyle(color: Color.fromRGBO(41, 75, 97, 1))),
              onPressed: () => play(true),
            ),
            MaterialButton(
              height: 40,
              color: Colors.white,
              child: const Text('play with a friend',
                  style: TextStyle(color: Color.fromRGBO(41, 75, 97, 1))),
              onPressed: () => play(false),
            ),
          ],
        ),
      ),
    );
  }

  void play(bool automate) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => Game(automate: automate)));
  }
}
