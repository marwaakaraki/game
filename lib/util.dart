import 'package:flutter/material.dart';

enum xo {
  X,
  O,
  none,
}

Widget mapImage(xo tileState) {
  switch (tileState) {
    case xo.O:
      return Image.asset('lib/images/o.png', color: Colors.white);
    case xo.X:
      return Image.asset('lib/images/x.png', color: Colors.white);
    case xo.none:
      return Container();
  }
}

/// this function will probably make you wish you were blind
/// if i had more time i'd look into the math and logic more
/// so i don't have to do all this
Map<bool, Set> evaluateWinnerr(List<int> xoList) {
  var xoListSet = xoList.toSet();
  if (xoListSet.containsAll({0, 1, 2})) {
    return {
      true: {0, 1, 2}
    };
  }
  if (xoListSet.containsAll({3, 4, 5})) {
    return {
      true: {3, 4, 5}
    };
  }
  if (xoListSet.containsAll({6, 7, 8})) {
    return {
      true: {6, 7, 8}
    };
  }
  if (xoListSet.containsAll({0, 3, 6})) {
    return {
      true: {0, 3, 6}
    };
  }
  if (xoListSet.containsAll({1, 4, 7})) {
    return {
      true: {1, 4, 7}
    };
  }
  if (xoListSet.containsAll({2, 5, 8})) {
    return {
      true: {2, 5, 8}
    };
  }
  if (xoListSet.containsAll({0, 4, 8})) {
    return {
      true: {0, 4, 8}
    };
  }
  if (xoListSet.containsAll({2, 4, 6})) {
    return {
      true: {2, 4, 6}
    };
  } else {
    return {false: {}};
  }
}
